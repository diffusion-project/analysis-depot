{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ebed5fc9",
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "from torch import nn\n",
    "import torch.nn.functional as F\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import torchvision.datasets as datasets\n",
    "import torchvision.transforms as transforms\n",
    "import math\n",
    "\n",
    "device = 'cuda' if torch.cuda.is_available() else 'cpu'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5fa8e8f",
   "metadata": {},
   "source": [
    "# Latent Diffusion Model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2818c4e",
   "metadata": {},
   "source": [
    "1. Introduction\n",
    "    - Super Resolution\n",
    "    - Conditioning Mechanism\n",
    "2. Class for the Latent Diffusion Model\n",
    "    - Initialization Arguments for our Diffusion Model\n",
    "    - Parameters of our Diffusion Model\n",
    "    - Latent Diffusion Model Class"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a792ac49",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37fa3ecd",
   "metadata": {},
   "source": [
    "We reach the state of the art in the world of diffusion models, namely the Latent Diffusion Model (LDM), introduced in the paper\n",
    "<br/><br/>\n",
    "\n",
    "<div align=\"center\">\n",
    "<a href=\"https://arxiv.org/abs/2207.12598\"><b>High-Resolution Image Synthesis With Latent Diffusion Models</b></a>\n",
    "</div>\n",
    "\n",
    "by Rombach et al. (2022). They propose using a **VQGAN** as an autoencoder to encode the images into a lower-dimensional, more computationally manageable, and perceptually equivalent latent space, where the diffusion process takes place. This dimensional reduction provides significant computational speedup, allowing for higher resolution image synthesis and learning more complex tasks.<br/>\n",
    "This is illustrated in the image shown below, where $E$ denotes the **VQGAN**'s encoder, projecting the input image $x_0$ into a learned latent space $z_0$, and $G$ the decoder providing a reconstruction of $x_0$ under the given latent encoding. The **VQGAN** does not need to be trained jointly with the diffusion model. We make use of two pretrained **VQGANs** from the repository <a href=\"https://github.com/CompVis/taming-transformers\">Taming Transformers for High-Resolution Image Synthesis</a> by Esser et al., which downsample the height and width of the image by a factor $f$ of $16$ and $8$ respectively. Every codevector has a dimension of $256$, resulting in latent encodings of shape $(256,32,32)$, for $f=16$, and $(256,64,64)$, for $f=8$, when dealing with $521^2px$ input images.\n",
    "<br/>\n",
    "<br/>\n",
    "Consequently, the diffusion model is now tasked to learn how to generate encodings from a pretrained latent space instead of the images themselves. The sampled images are then revealed through decoding the generated encodings. The decoding process involves a quantization layer that discretizes the given generated latent encoding from the diffusion model as a set of pretrained codevectors. This discretization process allows the diffusion model to slightly alter their generated encodings so as to further minimize the loss curve and improve its generative capabilities without impairing what it has already learned, since slight changes in encodings may still lead to the same codevector choice. This provides additional robustness to the training of the diffusion model. \n",
    "<br/>\n",
    "For more specifics on what type of **VQGAN** we used to train our models, we refer to the `image_encoding.ipynb` notebook.\n",
    "<br/><br/>\n",
    "Apart from the addition of the autoencoder, the diffusion model itself remains practically unchanged."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af9a9fd8",
   "metadata": {},
   "source": [
    "<p align=\"center\">\n",
    "  <img src=\"imgs/DM_3-1.png\" width=\"700\" height=\"200\">\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a6531e0",
   "metadata": {},
   "source": [
    "<u>**Super Resolution:**</u> <br/>\n",
    "<br/>\n",
    "Our focus is directed towards addressing the task of **Super Resolution** with our Latent Diffusion Model. In Super Resolution, the diffusion model is provided with a lower resolution image (e.g., $128x128px$) as conditioning, and augments its resolution by a factor of $4$ (to $512x512px$). This process doesn't simply upscale the image, as traditional interpolation algorithms do, instead, it learns how to add high-frequency details, yielding a sharp high-resolution version.<br/>\n",
    "<br/>\n",
    "We decided to train our LDM on the Landscapes (LHQ) dataset, which we have already used to train on our unconditional and inpainting models, applying the same transformations to the data. For more specifics on the data preprocessing, we refer to the `dataloading.ipynb` notebook.<br/>\n",
    "<br/>\n",
    "Once completely trained, one may sequentially apply **Super Resolution** to the outputs of the unconditional and inpainting models, effectively creating two distinct cascading diffusion models; High-Resolution Unconditional Landscape Image Synthesis and High-Resolution Landscape Inpainting.<br/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4bf05c93",
   "metadata": {},
   "source": [
    "<u>**Conditioning Mechanism:**</u> <br/>\n",
    "<br/>\n",
    "For Super Resolution, we are conditioning the diffusion model on the lower resolution $128x128px$ version of the landscape images. Similar to the conditioning mechanism we used in image inpainting, for the UNet, we also **concatenate** the low-res image with the input latent. However, recall that the input latent is an encoding in the latent space $(256,32,32)$ (or $(256,64,64)$) and would therefore not match the shape of the conditioning image $(3,128,128)$. To deal with this, we simply reshape the conditional image to match the latent input with a convolutional layer before **concatenating**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff3625cb",
   "metadata": {},
   "source": [
    "# Class for the Latent Diffusion Model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38ff6e39",
   "metadata": {},
   "source": [
    "We add the pretrained **VQGAN** as a parameter of the diffusion model. For memory management reasons on the HPC, we only store either the encoder or the decoder as part of the model. The encoder is only necessary in training, while the decoder is only used when sampling.\n",
    "<br/>\n",
    "<br/>\n",
    "As for the changes to the `forward` pass function of the latent diffusion model, it now receives a batch of clear training images from the dataloader as an argument, encodes them through the **VQGAN**, applies the forward trajectory to add noise, and determines the denoising distribution parameters with the UNet for the given timesteps and low resolution conditioning images.\n",
    "<br/>\n",
    "<br/>\n",
    "The training function remains the same. We made some changes regarding mixed precision training, etc. for everything to fit in one GPU. For more details, we refer to the `latent-diffusion` repository."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4199d98b",
   "metadata": {},
   "source": [
    "<u>**Initialization Arguments for our Diffusion Model**</u>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78a4d0a1",
   "metadata": {},
   "source": [
    "| LDM init arguments | description | \n",
    "| --- | --- | \n",
    "| net | U-Net to predict noise on the currnet noised latent |\n",
    "| diffusion_steps | Length of the Markov chain T|\n",
    "| out_shape | Shape of the models output images |\n",
    "| noise_schedule | Methods of initialization for the noise dist. variances; 'linear', 'cosine' or 'bounded_cosine' |\n",
    "| beta_1, beta_T | Variances for the first and last noise dist. (only for the 'linear' noise schedule) |\n",
    "| alpha_bar_lower_bound | Upper bound for the varaince of the total noise dist. (only for the 'cosine_bounded' noise schedule) |\n",
    "| var_schedule | Methods of initialization for the denoising dist. variances; 'same' or 'true'|\n",
    "| kl_loss | Choose between the mathematically correct 'weighted' or most commonly used 'simplified' KL loss |\n",
    "| recon_loss | 'none' to ignore the reconstruction loss, 'nll' to compute the negative log lekelihood|\n",
    "|guidance_score| Factor used in the classiffier-free guided diffusion linear interpolation when sampling|\n",
    "|class_free_guidence| Boolean value determining if classiffier-free guided diffusion is used for training and smapling |\n",
    "|vq_model| Pretrained VQGAN model used to encode or decode images|"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f62dcfa",
   "metadata": {},
   "source": [
    "For more detail on the already established arguments, we refer to the `unconditional_diffusion_model.ipynb` and  `conditional_diffusion_model.ipynb` notebooks."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12ae8ebe",
   "metadata": {},
   "source": [
    "<u>**Parameters of our Diffusion Model**</u>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ea8d7cb",
   "metadata": {},
   "source": [
    "| LDM class parameters | description | \n",
    "| --- | --- | \n",
    "| net | U-Net to predict noise on the currnet noised latent |\n",
    "| diffusion_steps | Length of the Markov chain T|\n",
    "| out_shape | Shape of the models output images |\n",
    "|beta| noise dist. hyperparam |\n",
    "|alpha| noise dist. hyperparam |\n",
    "|alpha_bar| noise dist. hyperparam |\n",
    "|var| varaince of the denoising dist.|\n",
    "|std| standatrd deviation of the denoising dist.|\n",
    "| noise_schedule | Methods of initialization for the noise dist. variances; 'linear', 'cosine' or 'bounded_cosine' |\n",
    "| var_schedule | Methods of initialization for the denoising dist. variances; 'same' or 'true'|\n",
    "|kl_loss | 'weighted' or 'simplified' KL loss |\n",
    "|recon_loss | 'none' or 'nll' reconstrcution loss|\n",
    "|sqrt_1_minus_alpha_bar| precomputed for for efficiency reasons|\n",
    "|sqrt_alpha_bar| precomputed for for efficiency reasons|\n",
    "|noise_scaler|to compute the mean of the dneoising dist., stored for efficiency reasons|\n",
    "|mean_scaler| to compute the mean of the dneoising dist., stored for efficiency reasons|\n",
    "|mse_weight| weight for the $L_t$ loss terms, stored for efficiency reasons|\n",
    "|guidance_score| factor used in the classiffier-free guided diffusion linear interpolation when sampling|\n",
    "|class_free_guidence| Boolean value determining if classiffier-free guided diffusion is used for training and smapling |\n",
    "|vq_model| Pretrained VQGAN model used to encode or decode images|"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e696707e",
   "metadata": {},
   "source": [
    "For more detail on the already established parameters, we refer to the `unconditional_diffusion_model.ipynb` and  `conditional_diffusion_model.ipynb` notebooks."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f12e720",
   "metadata": {},
   "source": [
    "<u>**Latent Diffusion Model Class**</u>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "811a81f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "class LDM(nn.Module):\n",
    "    \n",
    "    def __init__(self,\n",
    "                 vq_model,\n",
    "                 net=None,\n",
    "                 diffusion_steps = 50, \n",
    "                 out_shape = (256,32,32),\n",
    "                 noise_schedule = 'linear', \n",
    "                 beta_1 = 1e-4, \n",
    "                 beta_T = 0.02,\n",
    "                 alpha_bar_lower_bound = 0.9,\n",
    "                 var_schedule = 'same', \n",
    "                 kl_loss = 'simplified', \n",
    "                 recon_loss = 'none',\n",
    "                 device=None):\n",
    "        '''\n",
    "        net:                   UNet\n",
    "        diffusion_steps:       Length of the Markov chain\n",
    "        out_shape:             Shape of the UNet's models's in- and output images\n",
    "        noise_schedule:        Methods of initialization for the noise dist. variances, 'linear', 'cosine' or bounded_cosine\n",
    "        beta_1, beta_T:        Variances for the first and last noise dist. (only for the 'linear' noise schedule)\n",
    "        alpha_bar_lower_bound: Upper bound for the varaince of the complete noise dist. (only for the 'cosine_bounded' noise schedule)\n",
    "        var_schedule:          Options to initialize or learn the denoising dist. variances, 'same', 'true'\n",
    "        kl_loss:               Choice between the mathematically correct 'weighted' or in practice most commonly used 'simplified' KL loss\n",
    "        recon_loss:            Is 'none' to ignore the reconstruction loss or 'nll' to compute the negative log likelihood\n",
    "        '''\n",
    "        super(LDM,self).__init__()\n",
    "        self.device = device\n",
    "        # set the vqgan (encodes input when training and decodes the input when sampling)\n",
    "        self.vq_model = vq_model.to(device)\n",
    "        # initialize the beta's, alpha's and alpha_bar's for the given noise schedule\n",
    "        if noise_schedule == 'linear':\n",
    "            beta, alpha, alpha_bar = self.linear_schedule(diffusion_steps, beta_1, beta_T, device=self.device)\n",
    "        elif noise_schedule == 'cosine':\n",
    "            beta, alpha, alpha_bar = self.cosine_schedule(diffusion_steps, device=self.device)\n",
    "        elif noise_schedule == 'cosine_bounded':\n",
    "            beta, alpha, alpha_bar = self.bounded_cosine_schedule(diffusion_steps, alpha_bar_lower_bound, device=self.device)\n",
    "        else:\n",
    "            raise ValueError('Unimplemented noise scheduler')\n",
    "            \n",
    "        # initialize the denoising varainces for the given varaince schedule \n",
    "        if var_schedule == 'same':\n",
    "            var = beta\n",
    "        elif var_schedule == 'true':\n",
    "            var = [beta[0]] + [((1-alpha_bar[t-1])/(1-alpha_bar[t]))*beta[t] for t in range (1,diffusion_steps)]\n",
    "            var = torch.tensor(var, device=self.device)\n",
    "        else:\n",
    "            raise ValueError('Unimplemented variance scheduler')\n",
    "        \n",
    "        # check for invalid kl_loss argument\n",
    "        if (kl_loss != 'simplified') & (kl_loss != 'weighted'): \n",
    "            raise ValueError(\"Unimplemented loss function\")\n",
    "        \n",
    "        self.net = net\n",
    "        self.diffusion_steps = diffusion_steps\n",
    "        self.noise_schedule = noise_schedule\n",
    "        self.var_schedule = var_schedule\n",
    "        self.beta = beta\n",
    "        self.alpha = alpha\n",
    "        self.alpha_bar = alpha_bar\n",
    "        self.sqrt_1_minus_alpha_bar = torch.sqrt(1-alpha_bar) # for forward std\n",
    "        self.sqrt_alpha_bar = torch.sqrt(alpha_bar) # for forward mean\n",
    "        self.var = var\n",
    "        self.std = torch.sqrt(self.var)\n",
    "        self.kl_loss = kl_loss \n",
    "        self.recon_loss = recon_loss \n",
    "        self.out_shape = out_shape\n",
    "        # precomputed for efficiency reasons\n",
    "        self.noise_scaler = (1-alpha)/( self.sqrt_1_minus_alpha_bar)\n",
    "        self.mean_scaler = 1/torch.sqrt(self.alpha)\n",
    "        self.mse_weight = (self.beta**2)/(2*self.var*self.alpha*(1-self.alpha_bar))\n",
    "          \n",
    "    @staticmethod\n",
    "    def linear_schedule(diffusion_steps, beta_1, beta_T, device):\n",
    "        ''''\n",
    "        Function that returns the noise distribution hyperparameters for the linear schedule.  \n",
    "\n",
    "        Parameters:\n",
    "        diffusion_steps (int): Length of the Markov chain.\n",
    "        beta_1        (float): Variance of the first noise distribution.\n",
    "        beta_T        (float): Variance of the last noise distribution.\n",
    "        \n",
    "        Returns:\n",
    "        beta      (tensor): Linearly scaled from beta[0] = beta_1 to beta[-1] = beta_T, length is diffusion_steps.\n",
    "        alpha     (tensor): Length is diffusion_steps.\n",
    "        alpha_bar (tensor): Length is diffusion_steps.\n",
    "        '''\n",
    "        beta = torch.linspace(beta_1, beta_T, diffusion_steps,device=device)\n",
    "        alpha = 1 - beta\n",
    "        alpha_bar = torch.cumprod(alpha, dim=0)\n",
    "        return beta, alpha, alpha_bar\n",
    "    \n",
    "    @staticmethod    \n",
    "    def cosine_schedule(diffusion_steps, device):\n",
    "        '''\n",
    "        Function that returns the noise distribution hyperparameters for the cosine schedule.\n",
    "        From \"Improved Denoising Diffusion Probabilistic Models\" by Nichol and Dhariwal.\n",
    "\n",
    "        Parameters:\n",
    "        diffusion_steps (int): Length of the Markov chain.\n",
    "        \n",
    "        Returns:\n",
    "        beta      (tensor): Length is diffusion_steps.\n",
    "        alpha     (tensor): Length is diffusion_steps.\n",
    "        alpha_bar (tensor): Follows a sigmoid-like curve with a linear drop-off in the middle. \n",
    "                            Length is diffusion_steps.\n",
    "        '''\n",
    "        cosine_0 = LDM.cosine(torch.tensor(0, device=device), diffusion_steps= diffusion_steps)\n",
    "        alpha_bar = [LDM.cosine(torch.tensor(t, device=device),diffusion_steps = diffusion_steps)/cosine_0 \n",
    "                     for t in range(1, diffusion_steps+1)]\n",
    "        shift = [1] + alpha_bar[:-1]\n",
    "        beta = 1 - torch.div(torch.tensor(alpha_bar, device=device), torch.tensor(shift, device=device))\n",
    "        beta = torch.clamp(beta, min =0, max = 0.999).to(device) #suggested by paper\n",
    "        alpha = 1 - beta\n",
    "        alpha_bar = torch.tensor(alpha_bar,device=device)\n",
    "        return beta, alpha, alpha_bar\n",
    "    \n",
    "    @staticmethod    \n",
    "    def bounded_cosine_schedule(diffusion_steps, alpha_bar_lower_bound, device):\n",
    "        '''\n",
    "        Function that returns the noise distribution hyperparameters for our experimental version of a \n",
    "        bounded cosine schedule. Benefits are still unproven. It still has a linear drop-off in alpha_bar, \n",
    "        but it's not sigmoidal and the betas are no longer smooth.\n",
    "\n",
    "        Parameters:\n",
    "        diffusion_steps (int): Length of the Markov chain\n",
    "        \n",
    "        Returns:\n",
    "        beta      (tensor): Length is diffusion_steps\n",
    "        alpha     (tensor): Length is diffusion_steps\n",
    "        alpha_bar (tensor): Bounded between (alpha_bar_lower_bound, 1) with a linear drop-off in the middle. \n",
    "                            Length is diffusion_steps\n",
    "        '''\n",
    "        # get cosine alpha_bar (that range from 1 to 0)\n",
    "        _, _, alpha_bar = LDM.cosine_schedule(diffusion_steps, device)\n",
    "        # apply min max normalization on alpha_bar (range from lower_bound to 0.999)\n",
    "        min_val = torch.min(alpha_bar)\n",
    "        max_val = torch.max(alpha_bar)\n",
    "        alpha_bar = (alpha_bar - min_val) / (max_val - min_val)\n",
    "        alpha_bar = alpha_bar * (0.9999 - alpha_bar_lower_bound) + alpha_bar_lower_bound # for 0.9999=>beta_1 = 1e-4\n",
    "        # recompute beta, alpha and alpha_bar\n",
    "        alpha_bar = alpha_bar.tolist()\n",
    "        shift = [1] + alpha_bar[:-1]\n",
    "        beta = 1 - torch.div(torch.tensor(alpha_bar, device = device), torch.tensor(shift, device=device))\n",
    "        beta = torch.clamp(beta, min=0, max = 0.999)\n",
    "        beta = torch.tensor(sorted(beta), device = device)\n",
    "        alpha = 1 - beta\n",
    "        alpha_bar = torch.cumprod(alpha, dim=0)\n",
    "        return beta, alpha, alpha_bar\n",
    "\n",
    "    @staticmethod\n",
    "    def cosine(t, diffusion_steps, s = 0.008):\n",
    "        '''\n",
    "        Helper function that computes the cosine function from \"Improved Denoising Diffusion Probabilistic Models\" \n",
    "        by Nichol and Dhariwal, used for the cosine noise schedules.\n",
    "\n",
    "        Parameters:\n",
    "        t               (int): Current timestep\n",
    "        diffusion_steps (int): Length of the Markov chain\n",
    "        s             (float): Offset value suggested by the paper. Should be chosen such that sqrt(beta[0]) ~ 1/127.5 \n",
    "                               (for small T=50, this is not possible)\n",
    "                                \n",
    "        Returns:\n",
    "        (numpy.float64): Value of the cosine function at timestep t\n",
    "        '''\n",
    "        return (torch.cos((((t/diffusion_steps)+s)*math.pi)/((1+s)*2)))**2\n",
    "    \n",
    "    \n",
    "    ####\n",
    "    # Important to note: Timesteps are adjusted to the range t in [1, diffusion_steps] akin to the paper \n",
    "    # equations, where x_0 denotes the input image, z_0 its encoding through the VQGAN and z_t the noised \n",
    "    # latent after adding noise t times on z_0.\n",
    "    # Both trajectories are applied on batches assuming shape=(batch_size, channels, height, width).\n",
    "    ####\n",
    "    \n",
    "    # Forward Trajectory Functions:\n",
    "    \n",
    "    @torch.no_grad()\n",
    "    def forward_trajectory(self, z_0, t = None):\n",
    "        '''\n",
    "        Applies noise t times to each latent encoding in the batch z_0.\n",
    "        \n",
    "        Parameters:\n",
    "        z_0 (tensor): Batch of latent encodings\n",
    "        t   (tensor): Batch of timesteps, by default goes through full forward trajectory\n",
    "    \n",
    "        Returns:\n",
    "        z_T           (tensor): Batch of noised latents at timestep t\n",
    "        forward_noise (tensor): Batch of noise parameters from the noise distribution reparametrization used to draw z_T\n",
    "        '''\n",
    "        if t is None:\n",
    "            t = torch.full((z_0.shape[0],), self.diffusion_steps, device = self.device)\n",
    "        elif torch.any(t == 0):\n",
    "            raise ValueError(\"The tensor 't' contains a timestep zero.\")\n",
    "        forward_noise = torch.randn(z_0.shape, device = self.device)\n",
    "        z_T = self.noised_latent(forward_noise, z_0, t) \n",
    "        return z_T , forward_noise\n",
    "    \n",
    "    @torch.no_grad()\n",
    "    def noised_latent(self, forward_noise, z_0, t):\n",
    "        '''\n",
    "        Given a batch of noise parameters, this function recomputes the batch of noised latents at their respective timesteps t.\n",
    "        This allows us to avoid storing all the intermediate latents z_t along the forward trajectory.\n",
    "        \n",
    "        Parameters:\n",
    "        forward_noise (tensor): Batch of noise parameters from the noise distribution reparametrization used to draw z_t\n",
    "        z_0           (tensor): Batch of input latent encodings\n",
    "        t             (tensor): Batch of timesteps\n",
    "    \n",
    "        Returns:\n",
    "        x_t           (tensor): Batch of noised images at timestep t\n",
    "        '''\n",
    "        mean, std = self.forward_dist_param(z_0, t)\n",
    "        z_t = mean + std*forward_noise\n",
    "        return z_t\n",
    "    \n",
    "    @torch.no_grad()\n",
    "    def forward_dist_param(self, z_0, t):\n",
    "        '''\n",
    "        Computes the parameters of the complete noise distribution.\n",
    "        \n",
    "        Parameters:\n",
    "        z_0  (tensor): Batch of input latents\n",
    "        t    (tensor): Batch of timesteps\n",
    "    \n",
    "        Returns:\n",
    "        mean (tensor): Batch of means for the complete noise distribution for each image encoding in the batch z_0\n",
    "        std  (tensor): Batch of std scalars for the complete noise distribution for each image encoding in the batch z_0\n",
    "        '''\n",
    "        mean = self.sqrt_alpha_bar[t-1][:,None,None,None]*z_0\n",
    "        std = self.sqrt_1_minus_alpha_bar[t-1][:,None,None,None]\n",
    "        return mean, std\n",
    "    \n",
    "    @torch.no_grad()\n",
    "    def single_forward_dist_param(self, z_t_1, t):\n",
    "        '''\n",
    "        Computes the parameters of the individual noise distribution.\n",
    "\n",
    "        Parameters:\n",
    "        z_t_1 (tensor): Batch of noised image encodings at timestep t-1\n",
    "        t     (tensor): Batch of timesteps\n",
    "        \n",
    "        Returns:\n",
    "        mean (tensor): Batch of means for the individual noise distribution for each image in the batch z_t_1\n",
    "        std  (tensor): Batch of std scalars for the individual noise distribution for each image in the batch z_t_1\n",
    "        '''\n",
    "        mean = torch.sqrt(1-self.beta[t-1])[:,None,None,None]*z_t_1\n",
    "        std = torch.sqrt(self.beta[t-1])[:,None,None,None]\n",
    "        return mean, std\n",
    "\n",
    "\n",
    "    # Reverse Trajectory Functions:\n",
    "\n",
    "    def reverse_dist_param(self, z_t, t, y):\n",
    "        '''\n",
    "        Computes the parameters of the reverse dist.\n",
    "\n",
    "        Parameters:\n",
    "        z_t (tensor): Batch of input latent encodings\n",
    "        t   (tensor): Batch of timestep\n",
    "        y (tensor):   Batch of conditional information for each encoding in the batch z_t\n",
    "        \n",
    "        Returns:\n",
    "        z_t_1 (tensor): Batch of denoised image encodings at timestep t-1\n",
    "        '''\n",
    "        pred_noise = self.net(z_t,t,y)\n",
    "        mean = self.mean_scaler[t-1][:,None,None,None]*(z_t - self.noise_scaler[t-1][:,None,None,None]*pred_noise)\n",
    "        std = self.std[t-1][:,None,None,None]\n",
    "        return mean, std\n",
    "   \n",
    " \n",
    "    # Forward Function (primarily for training)\n",
    " \n",
    "    def forward(self, x_0, t, y):\n",
    "        '''\n",
    "        Encodes the input image x_0 with the model's VQGAN, applies noise to it t times, and computes the\n",
    "        parameters of its denoising distribution with the use of the UNet. Given the noised latent, timestep, and\n",
    "        conditioning information y, the UNet returns the predicted noise which is used to parameterize the mean and\n",
    "        std of the denoising Gaussian. \n",
    "        Since the LDM class is inheriting from the nn.Module class, and this is the forward pass used in training, \n",
    "        this function is required to share the name 'forward', even though we are effectively applying one reverse \n",
    "        step in the trajectory. \n",
    "\n",
    "        Parameters:\n",
    "        x_0 (tensor): Batch of input images, with color channels assumed to be normalized between [-1,1]\n",
    "        t   (tensor): Batch of timesteps\n",
    "        y (tensor):   Batch of conditional information for each input image\n",
    "        \n",
    "        Returns:\n",
    "        mean        (tensor): Batch of means for the complete noise dist. for each latent encoding in the batch z_t\n",
    "        std         (tensor): Batch of std scalars for the complete noise dist. for each latent encoding in the batch z_t\n",
    "        pred_noise  (tensor): Predicted noise for each latent encoding in the batch z_t\n",
    "        '''\n",
    "        with torch.no_grad():\n",
    "            z_0 = self.vq_model(x_0.to(self.device)).to(self.device)\n",
    "            z_t, forward_noise = self.forward_trajectory(z_0,t)\n",
    "        pred_noise = self.net(z_t,t,y)\n",
    "        mean = self.mean_scaler[t-1][:,None,None,None]*(z_t - self.noise_scaler[t-1][:,None,None,None]*pred_noise)\n",
    "        std = self.std[t-1][:,None,None,None]\n",
    "        return mean, std, pred_noise,forward_noise \n",
    "    \n",
    "    # Forward and Reverse Trajectory:\n",
    "\n",
    "    @torch.no_grad()\n",
    "    def complete_trajectory(self, x_0, y):\n",
    "        '''\n",
    "        Takes a batch of images, encodes them through the models VQGAN, and applies both trajectories sequentially, \n",
    "        i.e. first adds noise to all latent encodings along the forward chain and later removes the noise with the reverse chain.\n",
    "        This function will be used in the evaluation pipeline as a means to evaluate its performance on \n",
    "        how well it is able to reconstruct/recover the training images after applying the forward trajectory.\n",
    "\n",
    "        Parameters:\n",
    "        x_0 (tensor): Batch of input images, with color channels assumed to be normalized between [-1,1]\n",
    "        y (tensor):   Batch of conditional information for each input image\n",
    "        \n",
    "        Returns:\n",
    "        x_0_recon (tensor): Batch of images given by the model reconstruction of x_0\n",
    "        '''\n",
    "        if self.vq_model.coder == \"encoder\":\n",
    "            # apply encoder on the images to get latent encodings\n",
    "            z_0 = self.vq_model(x_0.to(self.device)).to(self.device)\n",
    "        else:\n",
    "            z_0=x_0\n",
    "        # apply forward trajectory\n",
    "        z_0_recon, _ = self.forward_trajectory(z_0)\n",
    "        # apply reverse trajectory\n",
    "        for t in reversed(range(1, self.diffusion_steps + 1)):\n",
    "            # draw noise used in the denoising dist. reparametrization\n",
    "            if t > 1:\n",
    "                noise = torch.randn(z_0_recon.shape, device=self.device)\n",
    "            else:\n",
    "                noise = torch.zeros(z_0_recon.shape, device=self.device)\n",
    "            # get denoising dist. param\n",
    "            mean, std = self.reverse_dist_param(z_0_recon, torch.full((z_0_recon.shape[0],), t, device = self.device), y)\n",
    "            # compute the drawn denoised latent at time t\n",
    "            z_0_recon = mean + std * noise\n",
    "        if self.vq_model.coder == \"decoder\":\n",
    "            # apply decoder on the reconstructed latent encoding to get original image back\n",
    "            return self.vq_model(z_0_recon.to(self.device)).to(self.device)\n",
    "        return z_0_recon \n",
    "      \n",
    "\n",
    "    # Sampling Functions:\n",
    "\n",
    "    @torch.no_grad()\n",
    "    def sample(self, y, batch_size = 10,  z_T=None):\n",
    "\n",
    "        '''\n",
    "        Samples batch_size images by passing a batch of randomly drawn noise parameters through the complete \n",
    "        reverse trajectory. The generated batch of latent encodings are finally decoded into the images with the model's VQGAN. \n",
    "        The last denoising step is deterministic as suggested by the paper \"Denoising Diffusion Probabilistic Models\" by Ho et al.\n",
    "\n",
    "        Parameters:\n",
    "        y (tensor):       Batch of conditional information for each input image\n",
    "        batch_size (int): Number of images to be sampled/generated from the diffusion model \n",
    "        x_T     (tensor): Input of the reverse trajectory. Batch of noised images usually drawn \n",
    "                          from an isotropic Gaussian, but can be set manually if desired.\n",
    "\n",
    "        Returns:\n",
    "        x_0 (tensor): Batch of sampled/generated images\n",
    "        '''\n",
    "        # start with a batch of isotropic noise images (or given arguemnt)\n",
    "        if z_T:\n",
    "            z_t_1 = z_T\n",
    "        else:\n",
    "            z_t_1 = torch.randn((batch_size,)+tuple(self.out_shape), device=self.device)\n",
    "        # apply reverse trajectory\n",
    "        for t in reversed(range(1, self.diffusion_steps+1)):\n",
    "            # draw noise used in the denoising dist. reparametrization\n",
    "            if t>1:\n",
    "                noise = torch.randn(z_t_1.shape, device=self.device)\n",
    "            else:\n",
    "                noise = torch.zeros(z_t_1.shape, device=self.device)\n",
    "            # get denoising dist. param\n",
    "            mean, std = self.reverse_dist_param(z_t_1, torch.full((z_t_1.shape[0],), t ,device = self.device), y)\n",
    "            # compute the drawn densoined latent at time t\n",
    "            z_t_1 = mean + std*noise\n",
    "        # quantize and decode the generated latent encoding\n",
    "        x_0 = self.vq_model.decode(z_t_1.to(self.device)).to(self.device)\n",
    "        return x_0\n",
    "\n",
    "    @torch.no_grad()\n",
    "    def sample_intermediates_latents(self, y):\n",
    "        '''\n",
    "        Samples a single latent encoding and provides all intermediate denoised images that were drawn along the reverse \n",
    "        trajectory. The generated abtch of encodings is finally decoded into a batch of images with the model's VQGAN.\n",
    "        The last denoising step is deterministic as suggested by the paper \"Denoising Diffusion Probabilistic Models\" by Ho et al.\n",
    "        \n",
    "        Parameters:\n",
    "        y (tensor): Batch of conditional information for each input image\n",
    "\n",
    "        Returns:\n",
    "        x (tensor): Contains the self.diffusion_steps+1 denoised image tensors\n",
    "        '''\n",
    "        # start with an image of pure noise (batch_size 1) and store it as part of the output \n",
    "        z_t_1 = torch.randn((1,) + tuple(self.out_shape), device=self.device)\n",
    "        z = torch.empty((self.diffusion_steps+1,) + tuple(self.out_shape), device=self.device)\n",
    "        z[-1] = z_t_1.squeeze(0)\n",
    "        # apply reverse trajectory\n",
    "        for t in reversed(range(1, self.diffusion_steps+1)):\n",
    "            # draw noise used in the denoising dist. reparametrization\n",
    "            if t>1:\n",
    "                noise = torch.randn(z_t_1.shape, device=self.device)\n",
    "            else:\n",
    "                noise = torch.zeros(z_t_1.shape, device=self.device)\n",
    "            # get denoising dist. param\n",
    "            mean, std = self.reverse_dist_param(z_t_1, torch.full((z_t_1.shape[0],), t ,device = self.device), y)\n",
    "            # compute the drawn densoined latent at time t\n",
    "            z_t_1 = mean + std*noise\n",
    "            # store noised image\n",
    "            z[t-1] = z_t_1.squeeze(0)\n",
    "        # quantize and decode the generated and all intermediate latent encodings\n",
    "        x = self.vq_model(z.to(self.device)).to(self.device)\n",
    "        return x\n",
    "\n",
    "\n",
    "    # Loss functions\n",
    "    \n",
    "    def loss_simplified(self, forward_noise, pred_noise, t=None):\n",
    "        '''\n",
    "        Returns the Mean Squared Error (MSE) between the forward_noise used to compute the noised latent images z_t \n",
    "        along the forward trajectory and the predicted noise computed by the U-Net with the noised latents z_t and timestep t.\n",
    "        '''\n",
    "        return F.mse_loss(forward_noise, pred_noise)\n",
    "    \n",
    "    \n",
    "    def loss_weighted(self, forward_noise, pred_noise, t):\n",
    "        '''\n",
    "        Returns the mathematically correct weighted version of the simplified loss.\n",
    "        '''\n",
    "        return self.mse_weight[t-1][:,None,None,None]*F.mse_loss(forward_noise, pred_noise)\n",
    "    \n",
    "    \n",
    "    # If t=0 and self.recon_loss == 'nll'\n",
    "    def loss_recon(self, z_0, mean_1, std_1):\n",
    "        '''\n",
    "        Returns the reconstruction loss given by the mean negative log-likelihood of z_0 under the last \n",
    "        denoising Gaussian distribution with mean mean_1 and standard deviation std_1.\n",
    "        '''\n",
    "        return -torch.distributions.Normal(mean_1, std_1).log_prob(z_0).mean()\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c5e40b8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.9 (pytorch)",
   "language": "python",
   "name": "pytorch"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
